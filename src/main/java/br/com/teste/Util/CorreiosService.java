package br.com.teste.Util;

import br.com.teste.Model.DTOs.ZipCodeDTO;
import br.com.teste.Model.Entity.ZipCodeEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class CorreiosService {

    @Value("${correios.consulta.cep}")
    private String UrlCorreios;

    private ValidatorCep validatorCep;

    private final RestTemplate restTemplate;

    public CorreiosService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ZipCodeEntity findByCep(String cep) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<ZipCodeDTO> request = new HttpEntity<>(headers);
        ZipCodeEntity zipCode = restTemplate.exchange(UrlCorreios + cep + "/json/", HttpMethod.GET, request, ZipCodeEntity.class).getBody();
        String zip = zipCode.getCep().replace("-","");
        zipCode.setCep(zip);
        return zipCode;
    }
}
