package br.com.teste.Util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
public class RestTemplateProperties {

    @Value("${resttemplate.readTimeout}")
    private Integer readTimeout;

    @Value("${resttemplate.connectionTimeout}")
    private Integer connectionTimeout;

    @Value("${resttemplate.password}")
    private String password;

    @Value("${resttemplate.protocol}")
    private String protocol;
}
