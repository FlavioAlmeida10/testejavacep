package br.com.teste.Model.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "TB_ZIP_CODE")
public class ZipCodeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZIP_ID")
    private long id;

    @Column(name = "ZIP_ZIPCODE",  unique = true)
    private String cep;

    @Column(name = "ZIP_PUBLICPLACE")
    private String logradouro;

    @Column(name = "ZIP_COMPLEMENT")
    private String complemento;

    @Column(name = "ZIP_NEIGHBORHOOD")
    private String bairro;

    @Column(name = "ZIP_LOCALITY")
    private String localidade;

    @Column(name = "ZIP_STATE")
    private String uf;

    @Column(name = "ZIP_IBGE")
    private String ibge;

    @Column(name = "ZIP_GIA")
    private String gia;

    @Column(name = "ZIP_DDD")
    private String ddd;

    @Column(name = "ZIP_SIAFI")
    private String siafi;

}

