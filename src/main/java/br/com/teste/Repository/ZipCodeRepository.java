package br.com.teste.Repository;

import br.com.teste.Model.Entity.ZipCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ZipCodeRepository extends JpaRepository<ZipCodeEntity, Long> {
    ZipCodeEntity findByCep(String cep);

}
