package br.com.teste.Service;

import br.com.teste.Model.DTOs.MessageResponseDTO;
import br.com.teste.Model.DTOs.ZipCodeDTO;
import br.com.teste.Model.Entity.ZipCodeEntity;
import br.com.teste.Repository.ZipCodeRepository;
import br.com.teste.Util.CorreiosService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ZipCodeService {

    @Autowired
    private CorreiosService correios;

    private ZipCodeRepository zipCodeRepository;

    private ModelMapper mapper = new ModelMapper();

    public ZipCodeService(CorreiosService correios, ZipCodeRepository zipCodeRepository) {
        this.correios = correios;
        this.zipCodeRepository = zipCodeRepository;
    }

    public ResponseEntity<?> findAll(){
        List<ZipCodeEntity> listZipCode = new ArrayList<ZipCodeEntity>();
        listZipCode = zipCodeRepository.findAll();
        if (listZipCode != null) {
            return ResponseEntity.status(HttpStatus.OK).body(listZipCode);
        } else {
            return ResponseEntity.badRequest().body(new MessageResponseDTO("Erro: CEP não encontrado!"));
        }
    }

    public ResponseEntity<?> findByCep(String cep) {
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.findByCep(cep);
        if (zipCode == null) {
            zipCode = correios.findByCep(cep);
            if (zipCode != null) {
                zipCode = zipCodeRepository.save(zipCode);
            } else {
                return ResponseEntity.badRequest().body(new MessageResponseDTO("Erro: CEP não encontrado!"));
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(zipCode);
    }

    public ResponseEntity<?> findById(long id){
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.getById(id);
        if(zipCode != null){
            return  ResponseEntity.status(HttpStatus.OK).body(zipCode);
        }else{
            return ResponseEntity.badRequest().body(new MessageResponseDTO("Erro:Id CEP não encontrado!"));
        }
    }

    public ResponseEntity<?> save(ZipCodeDTO zipCodeDTO) {
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.findByCep(zipCodeDTO.getCep());
        if (zipCode == null) {
            zipCode = mapper.map(zipCodeDTO, ZipCodeEntity.class);
            return ResponseEntity.status(HttpStatus.OK).body(zipCodeRepository.save(zipCode));
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(zipCode);
        }
    }

    public ResponseEntity<?> delete(String cep){
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.findByCep(cep);
        if(zipCode != null){
            zipCodeRepository.deleteById(zipCode.getId());
            return ResponseEntity.ok(new MessageResponseDTO("Sucesso: Ao deletar o numero do CEP!"));
        }else{
            return ResponseEntity.ok(new MessageResponseDTO("Erro: Ao deletar dados do CEP!"));
        }
    }

    public ResponseEntity<?> deleteId(long id){
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.getById(id);
        if(zipCode != null){
            zipCodeRepository.deleteById(zipCode.getId());
            return ResponseEntity.ok(new MessageResponseDTO("Sucesso: Ao deletar o numero do CEP!"));
        }else{
            return ResponseEntity.ok(new MessageResponseDTO("Erro: Ao deletar dados do CEP!"));
        }
    }

    public ResponseEntity<?> deleteEntity(ZipCodeDTO zipCodeDTO){
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.findByCep(zipCodeDTO.getCep());
        if(zipCode != null){
            zipCodeRepository.delete(zipCode);
            return ResponseEntity.ok(new MessageResponseDTO("Sucesso: CEP deletado com sucesso!"));
        }else{
            return ResponseEntity.ok(new MessageResponseDTO("Erro: Ao deletar dados do CEP pelo Model!"));
        }
    }


    public ResponseEntity<?> update(ZipCodeDTO zipCodeDTO){
        ZipCodeEntity zipCode = new ZipCodeEntity();
        zipCode = zipCodeRepository.findByCep(zipCodeDTO.getCep());
        if(zipCode != null){
            zipCode.setLogradouro(zipCodeDTO.getLogradouro());
            zipCode.setComplemento(zipCodeDTO.getComplemento());
            zipCode.setBairro(zipCodeDTO.getBairro());
            zipCode.setLocalidade(zipCodeDTO.getLocalidade());
            zipCode.setUf(zipCodeDTO.getUf());
            zipCode.setIbge(zipCodeDTO.getIbge());
            zipCode.setGia(zipCodeDTO.getGia());
            zipCode.setDdd(zipCodeDTO.getDdd());
            zipCode.setSiafi(zipCodeDTO.getSiafi());
            zipCode = zipCodeRepository.save(zipCode);
            return ResponseEntity.status(HttpStatus.OK).body(zipCode);
        }else {
            return ResponseEntity.ok(new MessageResponseDTO("Erro: CEP não localizado na base de dados!"));
        }
    }
}
