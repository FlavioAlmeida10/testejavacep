package br.com.teste.Controller;

import br.com.teste.Model.DTOs.MessageResponseDTO;
import br.com.teste.Model.DTOs.ZipCodeDTO;
import br.com.teste.Service.ZipCodeService;

import br.com.teste.Util.ValidatorCep;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "Cep Correios")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/cep")
public class ZipCodeController {
    @Autowired
    private ZipCodeService zipCodeService;

    private ValidatorCep validatorCep;

    public ZipCodeController(ZipCodeService zipCodeService, ValidatorCep validatorCep) {
        this.zipCodeService = zipCodeService;
        this.validatorCep = validatorCep;
    }

    @ApiOperation(value = "Buscar todos CEP já cadastrados")
    @GetMapping
    public ResponseEntity<?> getAll() {
        return zipCodeService.findAll();
    }

    @ApiOperation(value = "Buscar CEP na base ou Correios e cadastra na Base de Dados")
    @GetMapping("/{cep}")
    public ResponseEntity<?> search(@PathVariable String cep) {
        String zipcode = validatorCep.removeCaracter(cep);
        if (zipcode.length() < 8) {
            return ResponseEntity.badRequest().body(new MessageResponseDTO("Erro: CEP inválido!"));
        } else {
            return zipCodeService.findByCep(zipcode);
        }
    }

    @ApiOperation(value = "Buscar CEP na Base de Dados")
    @GetMapping("/searchId/{id}")
    public ResponseEntity<?> searchId(@PathVariable Long id) {
            return zipCodeService.findById(id);
    }

    @ApiOperation(value = "Cadastra CEP na Base de Dados pelo DTO")
    @PostMapping
    public ResponseEntity<?> save(@RequestBody ZipCodeDTO zipCodeDTO){
        String zipcode = validatorCep.removeCaracter(zipCodeDTO.getCep());
        zipCodeDTO.setCep(zipcode);
        return zipCodeService.save(zipCodeDTO);
    }

    @ApiOperation(value = "Atualiza CEP na Base de Dados pelo DTO")
    @PutMapping
    public ResponseEntity<?> update(@RequestBody ZipCodeDTO zipCodeDTO){
        String zipcode = validatorCep.removeCaracter(zipCodeDTO.getCep());
        zipCodeDTO.setCep(zipcode);
        return zipCodeService.update(zipCodeDTO);
    }

    @ApiOperation(value = "Deleta CEP da Base de Dados pelo CEP")
    @DeleteMapping("/{cep}")
    public ResponseEntity<?> deleteCep(@PathVariable String cep){
        String zipcode = validatorCep.removeCaracter(cep);
        return zipCodeService.delete(zipcode);
    }

    @ApiOperation(value = "Deleta CEP da Base de Dados pelo DTO")
    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteEntity(@RequestBody ZipCodeDTO zipCodeDTO){
        String zipcode = validatorCep.removeCaracter(zipCodeDTO.getCep());
        zipCodeDTO.setCep(zipcode);
        return zipCodeService.deleteEntity(zipCodeDTO);
    }

    @ApiOperation(value = "Deleta CEP da Base de Dados pelo Id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteEntity(@PathVariable long id){
        return zipCodeService.deleteId(id);
    }
}
